let collection = [];


function print(){
	return collection;
}

function enqueue(element){
	collection[collection.length] = element;

	return collection;
}


function dequeue(){
	delete collection[0];
	for(i=1;i<collection.length;i++){
		collection[i-1] = collection[i]
		delete collection[i];
	}
	collection.length = collection.length-1
	return collection;
}

function front(){
	return collection[0];
}

function size(){
	return collection.length;
}

function isEmpty(){
	if(collection.length == 0){
		return true
	}
	else{
		return false
	}
}

                                
module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty,
	collection
};